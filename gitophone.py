#!/usr/bin/env python3
#
# gitophone.py
#
# Copyright (C) 2019, 2020, 2022 Guillaume Tucker <guillaume.tucker@gmail.com>
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import json
import os
import math
import pygit2
import random
import splat
import splat.data
import splat.filters
import splat.gen
from splat import dB2lin as dB
import sys

# Constants
RATIOS = [2 / 3, 5 / 4, 9 / 8]
WEEK = 7 * 24 * 60 * 60

# Global variables
DEBUG = False
SEG_NUMBER = 0


class Commit:
    def __init__(self, obj):
        self._obj = obj
        self._parents = list()
        self._children = list()
        self._soid = None
        self._title = None
        self._segments = list()

    @property
    def obj(self):
        return self._obj

    @property
    def parents(self):
        return list(self._parents)

    @property
    def children(self):
        return list(self._children)

    @property
    def segments(self):
        return list(self._segments)

    @property
    def title(self):
        if self._title is None:
            self._title = self._obj.raw_message.splitlines()[0].decode()
        return self._title

    @property
    def soid(self):
        if self._soid is None:
            self._soid = str(self._obj.oid)[:12]
        return self._soid

    def add_parent(self, parent):
        self._parents.append(parent)

    def add_child(self, child):
        self._children.append(child)

    def add_segment(self, seg):
        self._segments.append(seg)


def _tail(tail, branches, obj, limit_obj):
    new_branches = set()
    limit = False
    while True:
        if obj.oid.hex in tail:
            break
        if limit_obj.oid == obj.oid:
            limit = True
        tail.add(obj.oid.hex)
        parent_objs = list(obj.parents)
        if not parent_objs:
            break
        obj = parent_objs.pop(0)
        if not limit and parent_objs:
            new_branches = new_branches.union(set(parent_objs))
    for bobj in new_branches.difference(branches):
        branches.add(bobj)
        _tail(tail, branches, bobj, limit_obj)


def create_tail(end_obj, limit_obj, tail_json, verbose):
    if tail_json and os.path.exists(tail_json):
        if verbose:
            print("  loading from {}".format(tail_json))
        with open(tail_json) as f:
            tail = set(json.load(f))
    else:
        tail = set()
        tail_branches = set()
        _tail(tail, tail_branches, end_obj, limit_obj)
        if tail_json:
            if verbose:
                print("  saving as {}".format(tail_json))
            with open(tail_json, 'w') as f:
                json.dump(list(tail), f, indent=' ')
    return tail


def _master(dataset, branches, repo, obj, end_obj, verbose=False):
    child = None
    while True:
        commit = Commit(obj)
        if child:
            commit.add_child(child)
            child.add_parent(commit)
        dataset[obj.oid.hex] = commit
        if DEBUG:
            print("{:05} {} {}".format(
                len(dataset), commit.soid, commit.title))
        parent_objs = list(obj.parents)
        if not parent_objs:
            break
        child = commit
        obj = parent_objs.pop(0)
        if parent_objs:
            branches[commit] = parent_objs
        if end_obj and end_obj.oid == child.obj.oid:
            break


def _branch(dataset, tail, repo, obj, end_obj, child, verbose=False):
    if DEBUG:
        print("--------------------------------------------------")
        print("--- {} {}".format(child.soid, child.title))

    branches = dict()
    while True:
        if obj.oid.hex in tail:
            if DEBUG:
                print("--- tail: {} {}".format(
                    obj.oid, obj.raw_message.splitlines()[0].decode()))
            break
        known_commit = dataset.get(obj.oid.hex)
        commit = known_commit or Commit(obj)
        if child:
            commit.add_child(child)
            child.add_parent(commit)
        if known_commit:
            if DEBUG:
                print("--- known: {} {}".format(commit.soid, commit.title))
            break
        dataset[obj.oid.hex] = commit
        if DEBUG:
            print("{:05} {} {}".format(
                len(dataset), commit.soid, commit.title))
        parent_objs = list(obj.parents)
        if not parent_objs:
            break
        child = commit
        obj = parent_objs.pop(0)
        branches[commit] = parent_objs

    for b_child, p_objs in branches.items():
        for p_obj in p_objs:
            _branch(dataset, tail, repo, p_obj, end_obj, b_child, verbose)


def create_dataset(start_obj, end_obj, repo, tail, verbose):
    dataset = dict()
    branches = dict()
    # Follow the first parents to get the "backbone" master branch
    _master(dataset, branches, repo, start_obj, end_obj, verbose)
    # Now go through all the merged branches
    for child, objs in branches.items():
        for obj in objs:
            _branch(dataset, tail, repo, obj, end_obj, child, verbose)
    return dataset


def find_segments(root, segments=None, merge=None):
    if segments is None:
        segments = dict()
    segment = list()
    branches = dict()
    if merge:
        segment.append(merge)
        merge.add_segment(segment)
    commit = root
    while True:
        segment.append(commit)
        commit.add_segment(segment)
        if not commit.parents:
            break
        if len(commit.parents) > 1:
            branches[commit] = commit.parents[1:]
        commit = commit.parents[0]
        if len(commit.segments):
            break
    segments[root] = segment
    if DEBUG:
        print("\n--- segment ---")
        for commit in segment:
            print("* {} {}".format(commit.soid, commit.title))
    for merge, parents in branches.items():
        for commit in parents:
            find_segments(commit, segments, merge)
    return segments


seg_number = 0


def make_timeline(root, segments):
    global SEG_NUMBER
    SEG_NUMBER += 1

    root_seg = segments[root]
    commits = sorted(root_seg, key=lambda a: a.obj.author.time)
    end = commits[-1]
    timeline = []
    previous = None
    mix_time = 0

    if DEBUG:
        print(f"{SEG_NUMBER:4d} {len(root_seg):3d} {root.soid} {root.title}")

    for i, co in enumerate(commits):
        if len(co.parents) > 1 and co != end:
            sub_timeline = make_timeline(co.parents[1], segments)
            timeline.append((mix_time, 0, co.obj.oid.hex, sub_timeline))
            continue

        elapsed = (
            co.obj.author.time - previous.obj.author.time
            if previous else 0
        )

        if elapsed:
            elapsed_log = math.log(max(elapsed, 1), WEEK)
            timeline.append((mix_time, elapsed_log, previous.obj.oid.hex, []))
            mix_time += elapsed_log

        previous = co

    return sorted(timeline)


def generate(timeline, dataset, freq, degree=1):
    frag = splat.data.Fragment()
    gen = splat.gen.TriangleGenerator(frag=frag)
    gen.filters = [splat.filters.linear_fade]
    balance = random.choice((0.1, -0.1)) * degree
    att = math.log(freq, 10000)
    levels = (dB(balance-att), dB(-balance-att))

    sub_frags = list()
    sub_offset = 0

    for mix_time, duration, oid, sub_timeline in timeline:
        if sub_timeline:
            next_freq = freq * random.choice(RATIOS)**degree
            if next_freq > 2000:
                next_freq /= 2
            elif next_freq < 60:
                next_freq *= 2
            sub_frag = generate(sub_timeline, dataset, next_freq, degree+1)
            sub_frags.append((mix_time, sub_frag))
            sub_offset = max(sub_offset, sub_frag.duration - mix_time)
        else:
            end = mix_time + (duration * 0.5)
            gen.run(freq=freq, start=mix_time, end=end, levels=levels)

    mix_frag = splat.data.Fragment()
    mix_frag.mix(frag, offset=sub_offset)
    for mix_time, sub_frag in sub_frags:
        mix_offset = mix_time + sub_offset - sub_frag.duration
        mix_debug = mix_offset + sub_frag.duration
        mix_frag.mix(sub_frag, offset=mix_offset)

    return mix_frag


def _get_obj(repo, rev):
    obj = repo.revparse_single(rev)
    if obj.type_str != 'commit':
        obj = obj.get_object()
    return obj


def main(args):
    global DEBUG
    if args.debug:
        DEBUG = True
    print("Repository: {}".format(args.path))
    repo = pygit2.Repository(args.path)
    start_obj = _get_obj(repo, args.start)
    end_obj = _get_obj(repo, args.end)
    limit_obj = _get_obj(repo, args.limit)
    if args.verbose:
        print("  repo:  {}".format(repo.path))
        print("  start: {}".format(start_obj.oid))
        print("  end:   {}".format(end_obj.oid))
        print("  limit: {}".format(limit_obj.oid))

    # Create the tail with commits prior to "end" and branches up to "limit"
    print("Creating tail...")
    tail = create_tail(end_obj, limit_obj, args.tail_json, args.verbose)
    if args.verbose:
        print("  tail:  {} commits".format(len(tail)))

    # Create the payload dataset with commits between "start" and "end"
    print("Creating dataset...")
    dataset = create_dataset(start_obj, end_obj, repo, tail, args.verbose)
    root = dataset[start_obj.oid.hex]
    if args.verbose:
        print("  dataset: {} commits".format(len(dataset)))
        print("  root: {} {}".format(root.soid, root.title))

    # Process the resulting graph
    print("Computing segments...")
    segments = find_segments(root)
    if args.verbose:
        print("  segments: {}".format(len(segments)))

    # Generate timeline from dataset
    print("Generating timeline...")
    timeline = make_timeline(root, segments)
    with open('timeline.json', 'w') as f:
        json.dump(timeline, f, indent='  ')

    # Generate audio from timeline
    print("Generating audio...")
    frag = generate(timeline, dataset, 440.0)
    frag.save("output.wav")

    return True


if __name__ == '__main__':
    parser = argparse.ArgumentParser("Generate audio from git history")
    parser.add_argument("path",
                        help="Path to a local git clone")
    parser.add_argument("--start", required=True,
                        help="Start revision")
    parser.add_argument("--end", required=True,
                        help="End revision")
    parser.add_argument("--limit", required=True,
                        help="Tail limit revision")
    parser.add_argument("--depth", type=int,
                        help="Maximum depth in each branch")
    parser.add_argument("--tail-json",
                        help="Path to JSON file with tail data")
    parser.add_argument("--verbose", action='store_true',
                        help="Verbose output")
    parser.add_argument("--debug", action='store_true',
                        help="Debug output")
    args = parser.parse_args(sys.argv[1:])
    res = main(args)
    sys.exit(0 if res is True else 1)
